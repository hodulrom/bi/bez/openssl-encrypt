#include <cstdlib>
#include <openssl/evp.h>
#include <cstring>

bool isHex(char c) {
    return (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F') || (c >= '0' && c <= '9');
}

unsigned char * parseEncrypted(char * str, size_t len) {
    unsigned char * encrypted = (unsigned char *) malloc(sizeof(unsigned char) * len);

    for (size_t i = 0; i < len * 2; i += 2) {
        // One byte in hexadecimal formatted string consist of two characters.
        char byteStr [3] = {str[i], str[i + 1], '\0'};

        if(!isHex(byteStr[0]) || !isHex(byteStr[1])) {
            return NULL;
        }

        // Convert to an actual byte.
        encrypted[i / 2] = (unsigned char) strtoul(byteStr, NULL, 16);
    }

    return encrypted;
}

int main(int argc, char * argv []) {
    unsigned char * plainText1;
    unsigned char plainText2[1024];
    unsigned char * encrypted1;
    unsigned char * encrypted2;
    size_t encrypted1Len;
    size_t encrypted2Len;
    size_t plainText1Len;

    if (argc != 4) {
        printf("usage: %s <string1> <string1_encrypted> <string2_encrypted>\n", argv[0]);
        return 1;
    }

    // Parse input
    plainText1Len = (size_t) strlen((const char *) argv[1]);
    encrypted1Len = (size_t) strlen((const char *) argv[2]);
    encrypted2Len = (size_t) strlen((const char *) argv[3]);

    if(encrypted1Len % 2 != 0) {
        encrypted1Len++;
    }
    if(encrypted2Len % 2 != 0) {
        encrypted2Len++;
    }

    encrypted1Len /= 2;
    encrypted2Len /= 2;

    plainText1 = (unsigned char *) argv[1];
    encrypted1 = parseEncrypted(argv[2], encrypted1Len);
    encrypted2 = parseEncrypted(argv[3], encrypted2Len);

    if(encrypted1 == NULL) {
        fprintf(stderr, "string1_encrypted is not a hexadecimal formatted string!\n");
        return 2;
    }
    if(encrypted2 == NULL) {
        fprintf(stderr, "string2_encrypted is not a hexadecimal formatted string!\n");
        return 2;
    }

    // Find the other plaintext
    size_t i;
    for (i = 0; (i < plainText1Len && i < encrypted1Len && i < encrypted2Len); ++i) {
        plainText2[i] = plainText1[i] ^ encrypted1[i] ^ encrypted2[i];
    }
    plainText2[i] = '\0';

    // Print first string
    printf("Encrypted: ");
    for(i = 0; i < encrypted1Len; i++) {
        printf("%02x", encrypted1[i]);
    }
    printf("\nDecrypted: %s\n", plainText1);

    // Second string
    printf("Encrypted: ");
    for(i = 0; i < encrypted2Len; i++) {
        printf("%02x", encrypted2[i]);
    }
    printf("\nDecrypted: %s\n", plainText2);

    // Release allocated resources
    free(encrypted1);
    free(encrypted2);

    return EXIT_SUCCESS;
}
