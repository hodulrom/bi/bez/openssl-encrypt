all:
	mkdir -p bin
	g++ -Wall main.cpp -lcrypto -o bin/main.o

run:
	./bin/main.o ${args}

clean:
	rm -rf bin
