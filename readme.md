# OpenSSL Cipher
Assignment number 2 of the 2nd laboratory. The program accepts 3 arguments: `plain-text`, `encrypted-text`, `encrypted-text2`.
Assuming the two encrypted strings are encrypted with RC4 with identical keys, `encrypted-text2` is decrypted and printed to standard output.

`encrypted-text2` can be decrypted up to the minimal length of the 3 given arguments.

[EDUX](https://edux.fit.cvut.cz/courses/BI-BEZ/labs/02/start)

## Usage
You can build and run the application via makefile.

**Build**
```bash
make
```
**Run**
```bash
# arguments: 
# 1st: plain text
# 2nd: encrypted plain text
# 3rd: encrypted unknown plain text with the same key
make args="'some plain text' 'f09989' '89c5a8'" run
```
**Clean up build files**
```
make clean
```

## Example
```bash
./bin/main.o \
    "Fešná paní volá: má málo vína pan šéf?" \
    "072252d08190258cae6eb3a0dbe0a07ab507ef08d70db389107ff3e0c6f099630b9d0d0ec1e4b8158c93f9d09b6d08" \
    "0b22fb14813cf2c5fe7fb41519e0b870b5a1241c" | tail -1 | cut -d ' ' -f 2-
# outputs: Jelenovi pivo nelej.

./bin/main.o \
    "Jelenovi pivo nelej." \
    "0b22fb14813cf2c5fe7fb41519e0b870b5a1241c" \
    "072252d08190258cae6eb3a0dbe0a07ab507ef08d70db389107ff3e0c6f099630b9d0d0ec1e4b8158c93f9d09b6d08" | tail -1 | cut -d ' ' -f 2-
# outputs: Fešná paní volá:
# the rest cannot be decrypted because the plain-text1 is shorter than full decrypted text.
```

## Return codes
* `0`: Success.
* `1`: Incorrect count of arguments.
* `2`: One or both of the encrypted strings are not in hexadecimal format.
